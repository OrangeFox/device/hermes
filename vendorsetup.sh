#
# Copyright 2015 The Android Open Source Project
#
# Copyright (C) 2019-2020 OrangeFox Recovery Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This file is executed by build/envsetup.sh, and can use anything
# defined in envsetup.sh.
#
# In particular, you can add lunch options with the add_lunch_combo
# function: add_lunch_combo generic-eng
#
FDEVICE="hermes"
if [ "$1" = "$FDEVICE" -o "$FOX_BUILD_DEVICE" = "$FDEVICE" ]; then
   export FOX_RECOVERY_INSTALL_PARTITION="/dev/block/mmcblk0p8"
   export FOX_USE_LZMA_COMPRESSION="0"
   export OF_DISABLE_MIUI_SPECIFIC_FEATURES="1"
   export OF_NO_RELOAD_AFTER_DECRYPTION="1"
   export OF_DONT_PATCH_ENCRYPTED_DEVICE="1"
   export OF_DONT_PATCH_ON_FRESH_INSTALLATION="1"
   export OF_LEGACY_MAGISKBOOT_ONLY="1"
   export OF_NO_MIUI_OTA_VENDOR_BACKUP="1"
   export OF_DISABLE_DM_VERITY="1"
     
   # let's see what are our build VARs
   if [ -n "$FOX_BUILD_LOG_FILE" -a -f "$FOX_BUILD_LOG_FILE" ]; then
  	   export | grep "FOX" >> $FOX_BUILD_LOG_FILE
  	   export | grep "OF_" >> $FOX_BUILD_LOG_FILE
  	   export | grep "TW_" >> $FOX_BUILD_LOG_FILE
  	   export | grep "TARGET_" >> $FOX_BUILD_LOG_FILE
   fi

add_lunch_combo omni_"$FDEVICE"-userdebug
fi
#
